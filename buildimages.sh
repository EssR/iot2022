#!/bin/bash
cd BokehCharts
docker image build . -t "datavisualization"
cd ../http.server
docker image build . -t "httpserver"
cd ../Project
docker compose up -d

