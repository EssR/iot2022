import requests
from http.server import HTTPServer, BaseHTTPRequestHandler
from bokeh.io import output_file
from bokeh.plotting import figure, show
from bokeh.models import DatetimeTickFormatter
from bokeh.resources import CDN
import numpy as np
from bokeh.embed import file_html
from datetime import datetime
import math
import os

"""
This is an implementation of the Bokeh library for data visualization, currently it is very simple
and merely displays a plotted graph over the latest 50 readings, it's sufficient for my needs
"""

# Environment variables
httpserverUrl = os.getenv('HTTPSERVERURL')
httpserverPort = os.getenv('HTTPSERVERPORT')
selfPort = os.getenv('SELFPORT')

def chart_maker():
    # request readings from the api
    dictionary = requests.get(f"http://{httpserverUrl}:{httpserverPort}").json()

    # trim dictionary when readings surpass 50, to get only relevant results
    if len(dictionary) > 50:
        slicer = slice(50)
        dictionary = dictionary[::-1][slicer][::-1]

    # reformat into a bokeh-usable dictionary
    # {'x': 'a', 'y': 'b'} --> {'a': 'b'} (keys disappear and values get mapped to each other)
    newDict = {}
    for x in dictionary:
        newDict[x["Time"]] = int(x["Moisture"])

    # assign axes for plotting
    xAxis = [key for key, value in newDict.items()]
    yAxis = [value for key, value in newDict.items()]

    # parse the dates in x-axis into Bokeh-usable
    formattedXAxis = []
    for x in xAxis:
        formattedXAxis.append(datetime.strptime(x, "%Y-%m-%dT%H:%M:%S%z"))

    print(formattedXAxis)

    # plot the chart
    fig = figure(title='Moisture readings over time',
                x_axis_type='datetime',
                plot_height=700, plot_width=1600,
                x_axis_label='Date', y_axis_label='Moisture',
                x_range=(formattedXAxis[0], formattedXAxis[(len(xAxis) -1)]), y_range=(0, 4095),
                toolbar_location=None)

    fig.line(x=formattedXAxis, y=yAxis, legend_label="Moisture level", line_width=2)

    fig.circle(x=formattedXAxis, y=yAxis,
            color='purple', size=10, alpha=0.5)

    # prettify the displayed date format
    fig.xaxis.formatter = DatetimeTickFormatter(hours=["%d %b %H:%M"],
                                                    days=["%d %b %H:%M"],
                                                    months=["%d %b %H:%M"],
                                                    years=["%d %b %H:%M"]
                                                    )
    fig.xaxis.major_label_orientation = math.pi/4

    # return html string representation of the plotted chart
    return file_html(fig, CDN, "Data visualization")


# Endpoints 
class requestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('content-type', 'text/html')
        self.end_headers()
        bokehhtml = chart_maker() # the returned html string displayed in browser
        output = ''
        output += bokehhtml
        self.wfile.write(output.encode()) 

# Initialization
def main():
    PORT = int(selfPort)
    server = HTTPServer(('', PORT), requestHandler)
    print('Server running on port %s' % PORT)
    print('')
    print('http://localhost:%s' % PORT)
    print('')
    server.serve_forever()

main()