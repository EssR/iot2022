from email import message
from http.server import HTTPServer, BaseHTTPRequestHandler
import json
import paho.mqtt.client as mqtt
from pymongo import MongoClient
import ast
import paho.mqtt.subscribe as subscribe
import paho.mqtt.publish as publish
from datetime import datetime
import threading
import time
import os
import telebot

"""
This http.server is an interface between the MQTT broker and the database using the common Paho-client

It formats readings sent from the ESP32 along with the current datetime into json format and saves to DB
It also serves the full contents of the database, i.e. previous readings, in case other components in the 
setup might be in need of this in the future
"""

# Environment variables
dbUrl = os.getenv('DBURL')
mqttUrl = os.getenv('MQTTURL')
username = os.getenv('MONGOUSER')
password = os.getenv('MONGOPASS')
dbPort = os.getenv('MONGOPORT')
telegramToken = os.getenv('TELEGRAMBOTTOKEN')
chat_id = os.getenv('TELEGRAMCHATID')

# configure telebot, her name is Gunhild :)
gunhild = telebot.TeleBot(telegramToken)

def gunhild_alert(waterLevel):
    gunhild.send_message(chat_id, f"Vattna krukan! Fuktnivå är {waterLevel} \U0001F4A6 \U0001FAB4")

# mongoDB
uri = f"mongodb://{username}:{password}@{dbUrl}:{dbPort}/?authMechanism=DEFAULT"
client = MongoClient(uri)
db = client.moisture
collection = db.measurements
error_db = client.errorlog
error_collection = error_db.logs

def get_measurements():
    # fetch the contents of db
    allMeasurements = list(collection.find({},{"Time": 1, "Moisture": 1, "_id": 0}))
    return json.dumps(allMeasurements)

# MQTT 
def on_connect(mqttc, obj, flags, rc):
    # give an indication that a connection has been established
    print("rc: " + str(rc))

def on_message(mqttc, obj, msg):

    # format datetime to appropriate iso-standard
    dateAndTime = datetime.now().astimezone().replace(microsecond=0).isoformat()

    # decode the message from bytes into string, otherwise incorrect format will be saved to DB
    msg.payload = msg.payload.decode("utf-8")

    # check that topic is not an error log, if so then proceed with saving to regular collection
    if msg.topic != "heltec/errorlog":

        # format the new insert into json-style
        newInsert = f"{{'Time': '{dateAndTime}', 'Moisture': '{msg.payload}'}}" 

        # insert into mongodb by first casting as dict type
        collection.insert_one(ast.literal_eval(newInsert))

        # if water level is too low, have bot send me a telegram message with the latest reading
        if int(msg.payload) < 200:
            gunhild_alert(msg.payload)

    # if topic is errorlog then save in error_collection
    else:
        newInsert = f"{{'Time': '{dateAndTime}', 'Topic': '{msg.topic}', 'Payload': '{msg.payload}'}}" 
        error_collection.insert_one(ast.literal_eval(newInsert))

def on_subscribe(mqttc, obj, mid, granted_qos):
    # give an indication that a subscription has been made
    print("Subscribed: " + str(mid) + " " + str(granted_qos))

def request_soilmoisture():
    # Event scheduling, to enable clientRequests every 30m or so
    while True:
        publish.single("httpserver/request", "soilmoisture", hostname=mqttUrl)
        print("Soil moisture sensor readings requested...")
        time.sleep(1800)

def request_temperature(): 
    # Identical to method definition above, but I don't use any other sensors at the moment so there is no thread created for this one
    while True:
        publish.single("httpserver/request", "temperature", hostname=mqttUrl)
        print("Temperature sensor readings requested...")
        time.sleep(1800)

def request_humidity(): 
    # Identical to method definition above, but I don't use any other sensors at the moment so there is no thread created for this one
    while True:
        publish.single("httpserver/request", "humidity", hostname=mqttUrl)
        print("Humidity sensor readings requested...")
        time.sleep(1800)

mqttc = mqtt.Client()                #
mqttc.on_connect = on_connect        #
mqttc.on_message = on_message        #
mqttc.on_subscribe = on_subscribe    # assign callback functions to prevent blocking
mqttc.connect(mqttUrl, 1883, 60)     #
mqttc.loop_start()                   #
mqttc.subscribe("heltec/sensor/soilmoisture", 0)
mqttc.subscribe("heltec/sensor/temperature", 0)
mqttc.subscribe("heltec/sensor/humidity", 0)
mqttc.subscribe("heltec/errorlog", 0)
mqttThread = threading.Thread(target=request_soilmoisture) # create a new thread for publishing periodic requests to prevent blocking

# Endpoints
class requestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        # Merely a response representing the contents of DB in json
        self.send_response(200)
        self.send_header('content-type', 'application/json')
        self.end_headers()
        readings = get_measurements()
        self.wfile.write(readings.encode()) # readings were defined as results of mongoDB-query earlier

# Initialization
def main():
    PORT = 9000
    server = HTTPServer(('', PORT), requestHandler)
    print('Server running on port %s' % PORT)
    print('')
    print('http://localhost:%s' % PORT)
    print('')
    mqttThread.start()
    server.serve_forever()

main()