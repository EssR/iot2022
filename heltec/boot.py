import machine
import time
import network
from network import WLAN
import ubinascii
from umqttsimple import MQTTClient
from machine import Pin
from machine import I2C
from machine import ADC
import envvars

"""
Beginning logic for connection to network, MQTT-broker, and sensors
These need to be initialized before anything else to ensure functionality
Currently oled display is commented out, maybe I'll use it in the future
"""
# Connect to sensor pins
adc = machine.ADC(0)
soil_moisture_pin = adc.channel(pin='P13', attn=ADC.ATTN_11DB) # 36 on board
temperature_pin = adc.channel(pin='P14', attn=ADC.ATTN_11DB) # 37 on board
humidity_pin = adc.channel(pin='P15', attn=ADC.ATTN_11DB) # 38 on board
other_pin = adc.channel(pin='P16', attn=ADC.ATTN_11DB) # 39 on board

# Start OLED
# oled_width = 128
# oled_height = 64
# OLED reset pin
# i2c_rst = Pin('P16', mode=Pin.OUT)
# Initialize the OLED display
# i2c_rst.value(0)
# time.sleep_ms(5)
# i2c_rst.value(1) # must be held high after initialization
# time.sleep_ms(500)
# Setup the I2C pins
# i2c_scl = Pin('P4', mode=Pin.OUT, pull=Pin.PULL_UP)
# i2c_sda = Pin('P3', mode=Pin.OUT, pull=Pin.PULL_UP)
# i2c = I2C(0, I2C.MASTER, baudrate=100000, pins=(i2c_sda,i2c_scl))
# time.sleep_ms(500)
# oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)
# oled.fill(0)
# oled.text('Welcome back', 10, 25)
# oled.show()

# setting up network
wlan = WLAN(mode=WLAN.STA)
try:
    # Initial attempt within try/catch in case network is unreachable
    wlan.connect(ssid=envvars.WIFI_NAME, auth=(WLAN.WPA2, envvars.WIFI_PASSWORD), timeout=7000)
    while not wlan.isconnected():
        machine.idle()
    print("WiFi connected successfully")
    print(wlan.ifconfig())
except Exception:
    # logic for other types of connections as fallback
    # TODO: add functionality for LoRaWan and setup gateway
    print("WiFi connection unsuccessful")

# setting up MQTT
mqtt_server = envvars.MQTT_SERVER # The MQTT-broker docker container's IP
client_id = ubinascii.hexlify(machine.unique_id()) # The Heltec will become an MQTT client
