import envvars

"""
General method definitions for MQTT functionality
"""
def restart_and_reconnect():
    """
    Restarting functionality for MQTT-broker in case of exception thrown
    """
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()

def connect_and_subscribe():
    """
    Connect and subscribe to receive clientRequests topic as part of Pull-pattern
    for passing measurements to client
    """
    global client_id, mqtt_server
    client = MQTTClient(client_id, mqtt_server, keepalive=60)
    client.set_callback(sub_cb)
    client.connect()
    print('Connected to %s MQTT broker' % (mqtt_server))
    for topic in envvars.TOPIC_SUB:
        client.subscribe(topic)
    return client

def sub_cb(topic, msg):
    """
    Callback method triggered when there is a client request for measurements
    Calls publish_measurements() to supply the latest measurements from sensors
    Callback is used instead of direct topic subscription because that is a
    blocking function

    :param topic: the received topic of the intercepted client request
    :param msg: the received msg of the intercepted client request
    """
    topic = topic.decode()
    msg = msg.decode()

    if msg == "soilmoisture":
        print("request: soil moisture readings")
        publish_pin13()

    elif msg == "temperature": # does not exist yet
        print("request: temperature readings")
        publish_pin14()

    elif msg == "humidity": # does not exist yet
        print("request: humidity readings")
        publish_pin15()

    else:
        print("request: not defined")
        publish_errorlog(topic, msg)

def publish_pin13():
    """
    To actually publish readings from the sensor to the topic
    """
    topic_pub = b'heltec/sensor/soilmoisture'
    try:
        value = str(4095-soil_moisture_pin())
        print(value)
        print("Attempting to send soil moisture")
        client.publish(topic_pub, value, retain=False, qos=0)
        time.sleep(5)
    except OSError as e:
        restart_and_reconnect()

def publish_pin14():
    """
    UNUSED
    """
    topic_pub = b'heltec/sensor/temperature'
    try:
        value = str(temperature_pin())
        print(value)
        print("Attempting to send temperature")
        client.publish(topic_pub, value, retain=False, qos=0)
        time.sleep(5)
    except OSError as e:
        restart_and_reconnect()

def publish_pin15():
    """
    UNUSED
    """
    topic_pub = b'heltec/sensor/humidity'
    try:
        value = str(humidity_pin())
        print(value)
        print("Attempting to send humidity")
        client.publish(topic_pub, value, retain=False, qos=0)
        time.sleep(5)
    except OSError as e:
        restart_and_reconnect()

def publish_pin16():
    """
    UNUSED
    """
    topic_pub = b'heltec/sensor/other'
    try:
        value = str(other_pin())
        print(value)
        print("Attempting to send other")
        client.publish(topic_pub, value, retain=False, qos=0)
        time.sleep(5)
    except OSError as e:
        restart_and_reconnect()

def publish_errorlog(topic, msg):
    """
    In case the incoming topic is undefined
    """
    topic_pub = b'heltec/errorlog'
    try:
        previous_request = topic + " " + msg
        print(previous_request)
        print("Attempting to send errorlog")
        client.publish(topic_pub, previous_request, retain=False, qos=0)
        time.sleep(5)
    except OSError as e:
        restart_and_reconnect()

"""
Initialization of program
"""
try:
    client = connect_and_subscribe()
except OSError as e:
    restart_and_reconnect()

while True:
    try:
        client.check_msg()
    except OSError as e:
        restart_and_reconnect()
